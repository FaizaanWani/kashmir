from dataclasses import asdict

from fastapi import FastAPI, Header

from kashmir.usecases import NewRelease, NewFix
from kashmir.providers import GitLab
from kashmir.repositories import ProjectRepository
from kashmir.core.data import Release, Version


app = FastAPI()


DEFAULT_SCM = "https://gitlab.com"


@app.post("/projects/{project_id}/releases", status_code=201)
async def new_release(project_id: int, scm: str = DEFAULT_SCM, token: str = Header(None)):
    scm = GitLab(
        server=scm,
        token=token,
        project_id=project_id
    )
    repository = ProjectRepository(scm)
    use_case = NewRelease(repository)
    release = use_case(project_id)
    return asdict(release)


@app.post("/projects/{project_id}/releases/{release}/fixes")
async def new_fix(project_id: int, release: str, scm: str = DEFAULT_SCM, token: str = Header(None)):
    scm = GitLab(
        server=scm,
        token=token,
        project_id=project_id
    )
    repository = ProjectRepository(scm)
    use_case = NewFix(repository)
    release = Release(project_id, Version.from_str(release))
    release = use_case(project_id, release)
    return asdict(release)
